package br.com.conductor.processamento.componentes;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.sshtools.net.SocketTransport;
import com.sshtools.sftp.SftpClient;
import com.sshtools.sftp.SftpFile;
import com.sshtools.ssh.PasswordAuthentication;
import com.sshtools.ssh.SshClient;
import com.sshtools.ssh.SshConnector;

@Component
public class FTPUtil {

	public Map<String, String> buscarArquivos(String servidor, Integer porta, String usuario, 
			String senha, String diretorioFtp, String diretorioLocalFtp) {

		SftpClient client = null;
		
		Map<String, String> files = new HashMap<>();
		try {
			client = conectarSFTP(servidor, porta, usuario, senha);
			SftpFile[] l = client.ls(diretorioFtp);
			for (SftpFile f : l) {
				if (f.isFile()) {
					client.get(f.getAbsolutePath(), diretorioLocalFtp + File.separator + f.getFilename());
					files.put(f.getFilename(), diretorioLocalFtp + File.separator + f.getFilename());
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
		
		return files;
	}
	
	public void moverArquivo(String servidor, Integer porta, String usuario, 
			String senha, String diretorioFtpExportacao, String diretorioFtpImportacao, String diretorioFtpInclusao, String nomeArquivo) {
		try {
			SftpClient sftp = conectarSFTP(servidor, porta, usuario, senha);
			sftp.get(diretorioFtpExportacao + File.separator + nomeArquivo, diretorioFtpInclusao);
			sftp.rm(diretorioFtpImportacao + File.separator + nomeArquivo);
			sftp.rm(diretorioFtpExportacao + File.separator + nomeArquivo);
		} catch (Exception e) {
			e.getMessage();
		} 
	}

	private SftpClient conectarSFTP(String servidor, Integer porta, String usuario, String senha)
			throws IOException, Exception {

		/**
		 * Create an SshConnector instance
		 */
		SshConnector con = SshConnector.createInstance();

		/**
		 * Connect to the host
		 */
		SocketTransport transport = new SocketTransport(servidor, porta);
		transport.setTcpNoDelay(true);
		SshClient ssh = con.connect(transport, usuario);

		/**
		 * Authenticate the user using password authentication
		 */
		PasswordAuthentication pwd = new PasswordAuthentication();
		pwd.setPassword(senha);
		ssh.authenticate(pwd);
		
		/**
		 * Start a session and do basic IO
		 */
		if (ssh.isAuthenticated()) {
			SftpClient sftp = new SftpClient(ssh);
			return sftp;
		}
		
		throw new Exception("Não foi possível conectar no host remoto.");
	}
}