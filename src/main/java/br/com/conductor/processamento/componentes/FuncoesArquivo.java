package br.com.conductor.processamento.componentes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class FuncoesArquivo {

	public List<StringBuilder> lerLinhasArquivo(String path, String extensao, String separador) {

		BufferedReader in = null;
		List<StringBuilder> list = null;
		try {
			try {
				File file = new File(path);
				in = new BufferedReader(new FileReader(file));
				list = new ArrayList<StringBuilder>();

				String str = null;
				while ((str = in.readLine()) != null) {
					list.add(new StringBuilder(str));
				}
				
			} finally {
				if (in != null) {
					in.close();
				}
			}
		} catch (IOException e) {
			e.getMessage();
		}

		return list;
	}
}
