package br.com.conductor.processamento.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitManager;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        basePackages = {"br.com.conductor.processamento.repository.api"},
        entityManagerFactoryRef = "apiEntityManager",
        transactionManagerRef = "apiTransactionManager"
)
public class ApiDatabaseConfiguration {

    private final PersistenceUnitManager persistenceUnitManager;


    public ApiDatabaseConfiguration(ObjectProvider<PersistenceUnitManager> persistenceUnitManager) {
        this.persistenceUnitManager = persistenceUnitManager.getIfAvailable();
    }

    @Bean
    @ConfigurationProperties("database.api.jpa")
    public JpaProperties apiJpaProperties() {
        return new JpaProperties();
    }

    @Bean
    @Primary
    @ConfigurationProperties("database.api.datasource")
    public HikariConfig apiHikariConfig() {
        return new HikariConfig();
    }

    @Bean
    @Primary
    @ConfigurationProperties("database.api.datasource.properties")
    public DataSource apiDataSource() {
        return new HikariDataSource(apiHikariConfig());
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean apiEntityManager(
            JpaProperties apiJpaProperties) {
        EntityManagerFactoryBuilder builder = createEntityManagerFactoryBuilder(
                apiJpaProperties);
        return builder
                .dataSource(apiDataSource())
                .packages("br.com.conductor.processamento.domain.api")
                .persistenceUnit("apiDs")
                .build();
    }

    @Bean
    @Primary
    public JpaTransactionManager apiTransactionManager(
            EntityManagerFactory apiEntityManager) {
        return new JpaTransactionManager(apiEntityManager);
    }

    private EntityManagerFactoryBuilder createEntityManagerFactoryBuilder(
            JpaProperties apiJpaProperties) {
        JpaVendorAdapter jpaVendorAdapter = createJpaVendorAdapter(apiJpaProperties);
        return new EntityManagerFactoryBuilder(jpaVendorAdapter,
                apiJpaProperties.getProperties(), this.persistenceUnitManager);
    }

    private JpaVendorAdapter createJpaVendorAdapter(JpaProperties apiJpaProperties) {
        AbstractJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setShowSql(apiJpaProperties.isShowSql());
        adapter.setDatabase(apiJpaProperties.getDatabase());
        adapter.setDatabasePlatform(apiJpaProperties.getDatabasePlatform());
        adapter.setGenerateDdl(apiJpaProperties.isGenerateDdl());
        return adapter;
    }

}
