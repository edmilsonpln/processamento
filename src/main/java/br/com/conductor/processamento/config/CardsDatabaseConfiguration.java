package br.com.conductor.processamento.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitManager;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        basePackages = {"br.com.conductor.processamento.repository.cards"},
        entityManagerFactoryRef = "cardsEntityManager",
        transactionManagerRef = "cardsTransactionManager"
)
public class CardsDatabaseConfiguration {

    private final PersistenceUnitManager persistenceUnitManager;


    public CardsDatabaseConfiguration(ObjectProvider<PersistenceUnitManager> persistenceUnitManager) {
        this.persistenceUnitManager = persistenceUnitManager.getIfAvailable();
    }

    @Bean
    @ConfigurationProperties("database.cards.jpa")
    public JpaProperties cardsJpaProperties() {
        return new JpaProperties();
    }

    @Bean
    @ConfigurationProperties("database.cards.datasource")
    public HikariConfig cardsHikariConfig() {
        return new HikariConfig();
    }

    @Bean
    @ConfigurationProperties("database.cards.datasource.properties")
    public DataSource cardsDataSource() {
        return new HikariDataSource(cardsHikariConfig());
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean cardsEntityManager(
            JpaProperties cardsJpaProperties) {
        EntityManagerFactoryBuilder builder = createEntityManagerFactoryBuilder(
                cardsJpaProperties);
        return builder
                .dataSource(cardsDataSource())
                .packages("br.com.conductor.processamento.domain.cards")
                .persistenceUnit("cardsDs")
                .build();
    }

    @Bean
    public JpaTransactionManager cardsTransactionManager(
            EntityManagerFactory cardsEntityManager) {
        return new JpaTransactionManager(cardsEntityManager);
    }

    private EntityManagerFactoryBuilder createEntityManagerFactoryBuilder(
            JpaProperties cardsJpaProperties) {
        JpaVendorAdapter jpaVendorAdapter = createJpaVendorAdapter(cardsJpaProperties);
        return new EntityManagerFactoryBuilder(jpaVendorAdapter,
                cardsJpaProperties.getProperties(), this.persistenceUnitManager);
    }

    private JpaVendorAdapter createJpaVendorAdapter(JpaProperties cardsJpaProperties) {
        AbstractJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setShowSql(cardsJpaProperties.isShowSql());
        adapter.setDatabase(cardsJpaProperties.getDatabase());
        adapter.setDatabasePlatform(cardsJpaProperties.getDatabasePlatform());
        adapter.setGenerateDdl(cardsJpaProperties.isGenerateDdl());
        return adapter;
    }

}
