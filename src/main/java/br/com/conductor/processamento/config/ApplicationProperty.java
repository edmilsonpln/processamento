package br.com.conductor.processamento.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("processamento")
public class ApplicationProperty {

	private String expression;

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}
}
