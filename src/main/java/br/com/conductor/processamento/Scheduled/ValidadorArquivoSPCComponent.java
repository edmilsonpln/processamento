package br.com.conductor.processamento.Scheduled;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.conductor.processamento.domain.api.Arquivo;
import br.com.conductor.processamento.domain.api.enuns.Status;
import br.com.conductor.processamento.repository.api.ArquivoRepositorio;
import br.com.conductor.processamento.service.ArquivoService;

@Component
public class ValidadorArquivoSPCComponent {

	private final Logger log = LoggerFactory.getLogger(ValidadorArquivoSPCComponent.class);
	
	private final ArquivoRepositorio arquivoRepositorio;
	private final ArquivoService arquivoService;
	
	public ValidadorArquivoSPCComponent(ArquivoRepositorio arquivoRepositorio, ArquivoService arquivoService) {
        this.arquivoRepositorio = arquivoRepositorio;
        this.arquivoService = arquivoService;
    }
	
	@Scheduled(initialDelay = 10000, cron = "${processamento.expression}")
	public void processarArquivos() {
		
		log.debug("Iniciando processamento de validação de arquivos");
		
		List<Arquivo> arquivos = this.arquivoRepositorio.findAllByStatus(Status.ATIVO);
		if (arquivos != null && !arquivos.isEmpty()) {
			log.debug("{} arquivos para processar", arquivos.size());
			arquivos.forEach(arquivo -> {
				arquivoService.validar(arquivo);
			});
		} else {
            log.debug("Sem arquivos para processar");
        }
		
        log.debug("Finalizando processamento de arquivos");
	}
}
