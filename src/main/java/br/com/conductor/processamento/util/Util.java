package br.com.conductor.processamento.util;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Util {
	
	public static SimpleDateFormat sdfData = new SimpleDateFormat("ddMMyyyy");

	public static BigDecimal getValor(String valor) {
		try {
			
			BigDecimal valorBd = new BigDecimal(retirarZeros(valor));
			valorBd.setScale(2,BigDecimal.ROUND_HALF_UP);
			return valorBd.equals(BigDecimal.ZERO) ? BigDecimal.ZERO : valorBd.divide(hundred(),2,BigDecimal.ROUND_HALF_EVEN);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return BigDecimal.ZERO;
	}
	
	public static String retirarZeros(String x) {
		String temp = "";
		for (int i = 0; i < x.length(); i++) {
			if (!x.substring(i, i + 1).equals("0")) { //checa se chegou no primeiro caracter q no  0
				temp += x.substring(i, x.length()); // temp fica com os valores correspondentes  substring da posio atual ate o fim
				break; // sai do lao
			}
		}
		return temp.equals("") ? "0":temp;
	}
	
	public static BigDecimal hundred() {
		return new BigDecimal("100");
	}
	
	public static Date getData(String data) throws ParseException {
		return sdfData.parse(data);
	}
	
	public static boolean check(String str) {
		return (str != null && !str.isEmpty());
	}
	
	public static Boolean isInteger(String integerAsString) {
		return getInteger(integerAsString, null) != null;
	}
	
	public static Integer getInteger(String src, Integer padrao) {
		if(src == null)
			return padrao;
		try {
			return Integer.parseInt(src);
		} catch(NumberFormatException e) {
			return padrao;
		}
	}
	public static NumberFormat getNf() {
		NumberFormat nf = NumberFormat.getInstance(Locale.ITALY);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		return nf;
	}
	
	
	
}
