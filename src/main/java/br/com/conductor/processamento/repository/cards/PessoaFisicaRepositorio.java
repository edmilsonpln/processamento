package br.com.conductor.processamento.repository.cards;

import br.com.conductor.processamento.domain.cards.PessoaFisica;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PessoaFisicaRepositorio extends JpaRepository<PessoaFisica, Long> {

    Optional<PessoaFisica> findByCpf(String cpf);
}
