package br.com.conductor.processamento.repository.api;

import java.util.List;

import br.com.conductor.processamento.domain.api.Arquivo;
import br.com.conductor.processamento.domain.api.enuns.Status;

public interface ArquivoRepositorio extends GenericRepository<Arquivo, Long> {

	 List<Arquivo> findAllByStatus(Status status);
}
