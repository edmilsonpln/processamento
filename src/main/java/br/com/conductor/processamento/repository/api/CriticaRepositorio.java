package br.com.conductor.processamento.repository.api;

import br.com.conductor.processamento.domain.api.Critica;

public interface CriticaRepositorio extends GenericRepository<Critica, Long> {

	Critica findOneByArquivoId(Long idArquivo);
}
