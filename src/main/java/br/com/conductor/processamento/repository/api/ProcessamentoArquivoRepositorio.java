package br.com.conductor.processamento.repository.api;

import br.com.conductor.processamento.domain.api.ProcessamentoArquivo;

public interface ProcessamentoArquivoRepositorio extends GenericRepository<ProcessamentoArquivo, Long> {
	
	ProcessamentoArquivo findOneByArquivoId(Long idArquivo);
}
