package br.com.conductor.processamento.domain.api;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.conductor.processamento.domain.api.enuns.Status;
import br.com.conductor.processamento.domain.api.enuns.TipoArquivo;
import br.com.conductor.processamento.domain.api.model.Model;

@Entity
@Table(name = "arquivo")
public class Arquivo extends Model<Long> {

	private static final long serialVersionUID = 1L;

	public static final String SEQUENCE_NAME = "s_arquivo";
	public static final String GENERATOR_NAME = "arquivo_gen";
	
	@Id
	@SequenceGenerator(name = GENERATOR_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
	@Column(name = "id")
	private Long id;
	
	@NotNull
	@Size(min = 3, max = 100)
	@Column(nullable = false, length = 100)
    private String emissor;
	
	@NotNull
	@Size(min = 3, max = 100)
	@Column(nullable = false, length = 100)
    private String descricao;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private Status status;
    
	@Valid
    @Embedded
    private ConfigFTP configFTP;
	
	@Enumerated(EnumType.STRING)
	@Column
	private TipoArquivo tipoArquivo;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = Critica.MAPPED_BY_ARQUIVO)
	private List<Critica> criticas = new ArrayList<>();
	
	public void addCriticas(Critica critica) {
		this.criticas.add(critica);
		critica.setArquivo(this);
	}

	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getEmissor() {
		return emissor;
	}

	public void setEmissor(String emissor) {
		this.emissor = emissor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public ConfigFTP getConfigFTP() {
		return configFTP;
	}

	public void setConfigFTP(ConfigFTP configFTP) {
		this.configFTP = configFTP;
	}

	public TipoArquivo getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(TipoArquivo tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}

	public List<Critica> getCriticas() {
		return criticas;
	}

	public void setCriticas(List<Critica> criticas) {
		this.criticas = criticas;
	}
}
