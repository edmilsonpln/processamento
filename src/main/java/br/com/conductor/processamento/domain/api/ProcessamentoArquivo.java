package br.com.conductor.processamento.domain.api;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import br.com.conductor.processamento.domain.api.model.Model;

public class ProcessamentoArquivo extends Model<Long>  {

	private static final long serialVersionUID = 1L;

	public static final String SEQUENCE_NAME = "s_processamento_arquivo";
	public static final String GENERATOR_NAME = "processamento_arquivo_gen";
	
	@Id
	@SequenceGenerator(name = GENERATOR_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
	@Column(name = "id")
	private Long id;
	
	@NotNull
    @ManyToOne
    @JoinColumn(name = "id_arquivo", nullable = false)
	private Arquivo arquivo;
	
	@Column(nullable = false)
	private LocalDateTime dataInicio;
	
	@Column(nullable = false)
	private LocalDateTime dataFim;
	
	@Column(nullable = true)
	private Integer tamanhoArquivo;
	
	@Column(nullable = true)
	private Integer totalLinhas;
	
	@Column(nullable = true)
	private Integer totalLinhasSucesso;
	
	@Column(nullable = true)
	private Integer totalLinhasCritica;
	
	public void addTotalLinhasSucesso() {
		setTotalLinhasSucesso(getTotalLinhas()-getTotalLinhasCritica());
	}
	
	public void addTotalLinhasCritica() {
		setTotalLinhasCritica(getTotalLinhasCritica()+1);
	}
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDateTime dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDateTime getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDateTime dataFim) {
		this.dataFim = dataFim;
	}

	public Integer getTamanhoArquivo() {
		return tamanhoArquivo;
	}

	public void setTamanhoArquivo(Integer tamanhoArquivo) {
		this.tamanhoArquivo = tamanhoArquivo;
	}

	public Integer getTotalLinhas() {
		return totalLinhas;
	}

	public void setTotalLinhas(Integer totalLinhas) {
		this.totalLinhas = totalLinhas;
	}

	public Integer getTotalLinhasSucesso() {
		return totalLinhasSucesso;
	}

	public void setTotalLinhasSucesso(Integer totalLinhasSucesso) {
		this.totalLinhasSucesso = totalLinhasSucesso;
	}

	public Integer getTotalLinhasCritica() {
		return totalLinhasCritica;
	}

	public void setTotalLinhasCritica(Integer totalLinhasCritica) {
		this.totalLinhasCritica = totalLinhasCritica;
	}
}
