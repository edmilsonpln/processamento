package br.com.conductor.processamento.domain.cards;

import br.com.conductor.processamento.domain.cards.enuns.Sexo;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "T_PESSOAFISICA")
@DynamicInsert
@DynamicUpdate
public class PessoaFisica  {
	
	private static final long serialVersionUID = 722243749660586321L;

	@Id
	@Column(name = "IDPESSOAFISICA")
	private Long id;
	
	@CPF
	@Column(name = "CPF")
	private String cpf;
	
	@Column(name = "RG")
	private String rg;

	@Column(name = "ORGAOEMISSOR")
	private String orgaoEmissor;

	@Column(name = "SEXO")
	private String sexo;

	@Column(name = "EMAIL")
	private String email;

	@ManyToOne
	@JoinColumn(name = "IDESTADOCIVIL")
	private EstadoCivil estadoCivil;

	@Column(name = "DATANASCIMENTO")
	private LocalDate dataNascimento;

	@Column(name = "CIDADENATAL")
	private String cidadeNatal;

	@Column(name = "ESTADONATAL")
	private String estadoNatal;

	@Column(name = "TEMPOEMPREGADO")
	private Integer tempoEmpregado;
	
	@ManyToOne
	@JoinColumn(name = "IDMORADIA")
	private Moradia moradia;
	
	@Column(name = "TEMPOMORADIAANOS")
	private Integer tempoMoradiaAnos;
	
	@Column(name = "TEMPOMORADIAMESES")
	private Integer tempoMoradiaMeses;

	@ManyToOne
	@JoinColumn(name = "IDESCOLARIDADE")
	private Escolaridade escolaridade;
	
	@Column(name = "DATAEMISSAORG")
	private LocalDate dataEmissaoRG;
	
	@Column(name = "NACIONALIDADE")
	private String nacionalidade;
	
	@ManyToOne
	@JoinColumn(name="IDOCUPACAO")
	private Ocupacao ocupacao;
	
	@Column(name = "DATAADMISSAOPROFISSAO")
	private LocalDate dataAdmissaoProfissao;
	
	@Column(name = "NUMEROBENEFICIO")
	private String numeroBeneficio;
	
	@ManyToOne
	@JoinColumn(name="IDPROFISSAO")
	private Profissao profissao;
	
	public void setSexo(Sexo sexo) {
		this.sexo = sexo.getSigla();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getOrgaoEmissor() {
		return orgaoEmissor;
	}

	public void setOrgaoEmissor(String orgaoEmissor) {
		this.orgaoEmissor = orgaoEmissor;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getCidadeNatal() {
		return cidadeNatal;
	}

	public void setCidadeNatal(String cidadeNatal) {
		this.cidadeNatal = cidadeNatal;
	}

	public String getEstadoNatal() {
		return estadoNatal;
	}

	public void setEstadoNatal(String estadoNatal) {
		this.estadoNatal = estadoNatal;
	}

	public Integer getTempoEmpregado() {
		return tempoEmpregado;
	}

	public void setTempoEmpregado(Integer tempoEmpregado) {
		this.tempoEmpregado = tempoEmpregado;
	}

	public Moradia getMoradia() {
		return moradia;
	}

	public void setMoradia(Moradia moradia) {
		this.moradia = moradia;
	}

	public Integer getTempoMoradiaAnos() {
		return tempoMoradiaAnos;
	}

	public void setTempoMoradiaAnos(Integer tempoMoradiaAnos) {
		this.tempoMoradiaAnos = tempoMoradiaAnos;
	}

	public Integer getTempoMoradiaMeses() {
		return tempoMoradiaMeses;
	}

	public void setTempoMoradiaMeses(Integer tempoMoradiaMeses) {
		this.tempoMoradiaMeses = tempoMoradiaMeses;
	}

	public Escolaridade getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(Escolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}

	public LocalDate getDataEmissaoRG() {
		return dataEmissaoRG;
	}

	public void setDataEmissaoRG(LocalDate dataEmissaoRG) {
		this.dataEmissaoRG = dataEmissaoRG;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public Ocupacao getOcupacao() {
		return ocupacao;
	}

	public void setOcupacao(Ocupacao ocupacao) {
		this.ocupacao = ocupacao;
	}

	public LocalDate getDataAdmissaoProfissao() {
		return dataAdmissaoProfissao;
	}

	public void setDataAdmissaoProfissao(LocalDate dataAdmissaoProfissao) {
		this.dataAdmissaoProfissao = dataAdmissaoProfissao;
	}

	public String getNumeroBeneficio() {
		return numeroBeneficio;
	}

	public void setNumeroBeneficio(String numeroBeneficio) {
		this.numeroBeneficio = numeroBeneficio;
	}

	public Profissao getProfissao() {
		return profissao;
	}

	public void setProfissao(Profissao profissao) {
		this.profissao = profissao;
	}
}
