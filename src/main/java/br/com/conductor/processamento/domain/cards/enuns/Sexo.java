package br.com.conductor.processamento.domain.cards.enuns;

public enum Sexo {
	MASCULINO("M"),
	FEMININO("F");
	
	private final String sigla;
	
	private Sexo(final String sigla) {
		if (sigla == null) {
			throw new IllegalArgumentException();
		}
		
		this.sigla = sigla;
	}

	public String getSigla() {
		return sigla;
	}
	
}
