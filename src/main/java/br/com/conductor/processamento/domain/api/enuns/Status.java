package br.com.conductor.processamento.domain.api.enuns;

public enum Status {

	ATIVO, INATIVO;
}
