package br.com.conductor.processamento.domain.api;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import br.com.conductor.processamento.domain.api.model.Model;

@Entity
@Table(name = "critica")
public class Critica extends Model<Long> {

	private static final long serialVersionUID = 1L;

	public static final String SEQUENCE_NAME = "s_critica";
	public static final String GENERATOR_NAME = "critica_gen";

	public static final String MAPPED_BY_ARQUIVO = "arquivo";

	@Id
	@SequenceGenerator(name = GENERATOR_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
	@Column(name = "id")
	private Long id;
	
	@NotNull
    @ManyToOne
    @JoinColumn(name = "id_arquivo", nullable = false)
	private Arquivo arquivo;

	@NotNull
	@Column(nullable = false)
	private Integer linha;
	
	@NotNull
	@Column(nullable = false)
	private String mensagem;	
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public Integer getLinha() {
		return linha;
	}

	public void setLinha(Integer linha) {
		this.linha = linha;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
