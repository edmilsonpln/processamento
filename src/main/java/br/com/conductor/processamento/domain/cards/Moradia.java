package br.com.conductor.processamento.domain.cards;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "T_MORADIA")
public class Moradia {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "IDMORADIA")
	private Long id;
	
	@NotNull
	@Column(name = "DESCRICAO")
	private String descricao;
	
	@Column(name = "PONTUACAO")
	private Integer pontuacao;
	
	@Column(name = "CODIGOEXTERNO")
	private Integer codigoExterno;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(Integer pontuacao) {
		this.pontuacao = pontuacao;
	}

	public Integer getCodigoExterno() {
		return codigoExterno;
	}

	public void setCodigoExterno(Integer codigoExterno) {
		this.codigoExterno = codigoExterno;
	}
	
}
