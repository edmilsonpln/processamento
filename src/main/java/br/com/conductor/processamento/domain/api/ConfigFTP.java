package br.com.conductor.processamento.domain.api;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
public class ConfigFTP implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 3, max = 100)
	@Column(nullable = false, length = 100)
    private String hostFtp;
    
	@NotNull
	@Column(nullable = false)
    private Integer portaFtp;
    
	@NotNull
	@Size(min = 3, max = 100)
	@Column(nullable = false, length = 100)
    private String diretorioFtpExportacao;
    
	@NotNull
	@Size(min = 3, max = 100)
	@Column(nullable = false, length = 100)
    private String diretorioFtpImportacao;
    
	@NotNull
	@Size(min = 3, max = 100)
	@Column(nullable = false, length = 100)
    private String diretorioFtpInclusao;
	
	@NotNull
	@Size(min = 3, max = 50)
	@Column(nullable = false, length = 50)
    private String usuarioFtp;
    
	@NotNull
	@Size(min = 3, max = 100)
	@Column(nullable = false, length = 100)
    private String senhaFtp;

	public String getHostFtp() {
		return hostFtp;
	}

	public void setHostFtp(String hostFtp) {
		this.hostFtp = hostFtp;
	}

	public Integer getPortaFtp() {
		return portaFtp;
	}

	public void setPortaFtp(Integer portaFtp) {
		this.portaFtp = portaFtp;
	}

	public String getDiretorioFtpExportacao() {
		return diretorioFtpExportacao;
	}

	public void setDiretorioFtpExportacao(String diretorioFtpExportacao) {
		this.diretorioFtpExportacao = diretorioFtpExportacao;
	}

	public String getDiretorioFtpImportacao() {
		return diretorioFtpImportacao;
	}

	public void setDiretorioFtpImportacao(String diretorioFtpImportacao) {
		this.diretorioFtpImportacao = diretorioFtpImportacao;
	}

	public String getDiretorioFtpInclusao() {
		return diretorioFtpInclusao;
	}

	public void setDiretorioFtpInclusao(String diretorioFtpInclusao) {
		this.diretorioFtpInclusao = diretorioFtpInclusao;
	}

	public String getUsuarioFtp() {
		return usuarioFtp;
	}

	public void setUsuarioFtp(String usuarioFtp) {
		this.usuarioFtp = usuarioFtp;
	}

	public String getSenhaFtp() {
		return senhaFtp;
	}

	public void setSenhaFtp(String senhaFtp) {
		this.senhaFtp = senhaFtp;
	}
}
