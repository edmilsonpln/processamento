package br.com.conductor.processamento.domain.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;

@MappedSuperclass
public abstract class Model<T extends Serializable> implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	public abstract T getId();

	public abstract void setId(T id);

	@Column(updatable = false, name = "creation_date")
	private LocalDateTime creationDate;
	
	@PrePersist
	public void prePersist() {
		this.setCreationDate(LocalDateTime.now());
	}
	
	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Model<?> other = (Model<?>) obj;

        T id = getId();

        if (id == null) {
            if (other.getId() != null)
                return false;
        } else if (!id.equals(other.getId()))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "id=" + getId();
    }
}
