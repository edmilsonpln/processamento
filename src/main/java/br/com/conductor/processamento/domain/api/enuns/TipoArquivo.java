package br.com.conductor.processamento.domain.api.enuns;

public enum TipoArquivo {

	SPC_BRASIL, SOPHUS, TPC, ACP;
}
