package br.com.conductor.processamento.service;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.conductor.processamento.domain.api.Arquivo;
import br.com.conductor.processamento.domain.api.ProcessamentoArquivo;
import br.com.conductor.processamento.repository.api.ProcessamentoArquivoRepositorio;

@Service
	
public class ProcessamentoArquivoServiceImpl implements ProcessamentoArquivoService {

	private final ProcessamentoArquivoRepositorio processamentoArquivoRepositorio;
	
	public ProcessamentoArquivoServiceImpl(ProcessamentoArquivoRepositorio processamentoArquivoRepositorio) {
		this.processamentoArquivoRepositorio = processamentoArquivoRepositorio;
	}

	@Transactional
	public ProcessamentoArquivo getProcessamentoArquivo(Arquivo arquivo) {
		ProcessamentoArquivo processamentoArquivo = this.processamentoArquivoRepositorio.findOneByArquivoId(arquivo.getId());
		if (processamentoArquivo != null)
			return processamentoArquivo;
		return cadastrarProcessamentoArquivo(arquivo);
	}

	private ProcessamentoArquivo cadastrarProcessamentoArquivo(Arquivo arquivo) {
		final ProcessamentoArquivo processamentoArquivo = new ProcessamentoArquivo();
		processamentoArquivo.setDataInicio(LocalDateTime.now());
		processamentoArquivo.setArquivo(arquivo);
		this.salvar(processamentoArquivo);
		return processamentoArquivo;
	}

	@Override
	@Transactional
	public void salvar(ProcessamentoArquivo processamentoArquivo) {
		this.processamentoArquivoRepositorio.save(processamentoArquivo);
	}	
}
