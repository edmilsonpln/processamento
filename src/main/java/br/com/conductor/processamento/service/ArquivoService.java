package br.com.conductor.processamento.service;

import br.com.conductor.processamento.domain.api.Arquivo;

public interface ArquivoService{
	
	void validar(Arquivo arquivo);
}
