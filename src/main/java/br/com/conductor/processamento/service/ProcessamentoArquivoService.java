package br.com.conductor.processamento.service;

import br.com.conductor.processamento.domain.api.Arquivo;
import br.com.conductor.processamento.domain.api.ProcessamentoArquivo;

public interface ProcessamentoArquivoService{
	
	ProcessamentoArquivo getProcessamentoArquivo(Arquivo arquivo);
	
	void salvar(ProcessamentoArquivo processamentoArquivo);
}
