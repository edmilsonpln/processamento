package br.com.conductor.processamento.service;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.conductor.processamento.componentes.FTPUtil;
import br.com.conductor.processamento.componentes.FuncoesArquivo;
import br.com.conductor.processamento.domain.api.Arquivo;
import br.com.conductor.processamento.domain.api.ConfigFTP;
import br.com.conductor.processamento.domain.api.Critica;
import br.com.conductor.processamento.domain.api.ProcessamentoArquivo;
import br.com.conductor.processamento.exeption.CriticaException;
import br.com.conductor.processamento.service.validadores.ValidadorFactory;
import br.com.conductor.processamento.service.validadores.ValidadorService;

@Service
public class ArquivoServiceImpl implements ArquivoService {
	
	private final FTPUtil ftp;
	private final FuncoesArquivo funcoesArquivo; 
	private final ProcessamentoArquivoService processamentoArquivoService;
	private final CriticaService criticaService;
	private final ValidadorFactory validadorFactory;
	
	public ArquivoServiceImpl(FTPUtil ftp, FuncoesArquivo funcoesArquivo, ProcessamentoArquivoService processamentoArquivoService, 
			CriticaService criticaService, ValidadorFactory validadorFactory) {
		this.ftp = ftp;
		this.funcoesArquivo = funcoesArquivo;
		this.processamentoArquivoService = processamentoArquivoService;
		this.criticaService = criticaService;
		this.validadorFactory = validadorFactory;
	}

	@Override
	@Transactional
	public void validar(Arquivo arquivo) {
		
		ProcessamentoArquivo processamentoArquivo = processamentoArquivoService.getProcessamentoArquivo(arquivo);
		
		ConfigFTP configFTP = arquivo.getConfigFTP();
		Map<String, String> arquivos = ftp.buscarArquivos(configFTP.getHostFtp(), 
														  configFTP.getPortaFtp(), 
														  configFTP.getUsuarioFtp(), 
													      configFTP.getSenhaFtp(), 
														  configFTP.getDiretorioFtpExportacao(), 
														  configFTP.getDiretorioFtpImportacao());
		
		for (String nomeArquivo : arquivos.keySet()) {
			
			processarValidadorArquivo(arquivos.get(nomeArquivo), arquivo, processamentoArquivo);
			
			// Move arquivo da pasta importado para remessa, caso não tenha critica
			if (arquivo.getCriticas().isEmpty()) {
				ftp.moverArquivo(configFTP.getHostFtp(), 
								 configFTP.getPortaFtp(), 
								 configFTP.getUsuarioFtp(),  
								 configFTP.getSenhaFtp(), 
								 configFTP.getDiretorioFtpExportacao(),
								 configFTP.getDiretorioFtpImportacao(),
								 configFTP.getDiretorioFtpInclusao(),
								 nomeArquivo);
			}
		}
		
		processamentoArquivo.setDataFim(LocalDateTime.now());
		this.processamentoArquivoService.salvar(processamentoArquivo);
	}
	
	private Arquivo processarValidadorArquivo(String nomeArquivo, Arquivo arquivo, ProcessamentoArquivo processamentoArquivo) {
		
		List<StringBuilder> linhas = funcoesArquivo.lerLinhasArquivo(nomeArquivo, "txt", null);
		processamentoArquivo.setTotalLinhas(linhas.size());
		
		Integer numeroLinha = 1;
		for (StringBuilder linha : linhas) {
			try {
				if (linha != null) {
					ValidadorService validadorService = validadorFactory.getValidador(arquivo.getTipoArquivo());
					validadorService.validar(linha.toString());
				}
			} catch(CriticaException e) {
				processamentoArquivo.addTotalLinhasCritica();
				Critica critica = this.criticaService.cadastrar(arquivo, numeroLinha, e.getMessage());
				arquivo.addCriticas(critica);
			}
			
			numeroLinha = numeroLinha + 1;
		}
		
		processamentoArquivo.addTotalLinhasSucesso();
		this.processamentoArquivoService.salvar(processamentoArquivo);
		return arquivo;
	}
}
