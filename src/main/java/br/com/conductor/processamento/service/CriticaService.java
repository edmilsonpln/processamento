package br.com.conductor.processamento.service;

import br.com.conductor.processamento.domain.api.Arquivo;
import br.com.conductor.processamento.domain.api.Critica;

public interface CriticaService {
	
	public Critica cadastrar(Arquivo arquivo, Integer linha, String mensagem);
}
