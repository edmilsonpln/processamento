package br.com.conductor.processamento.service.validadores.spcbrasil;

import java.text.ParseException;
import java.util.Optional;

import br.com.conductor.processamento.domain.cards.PessoaFisica;
import br.com.conductor.processamento.exeption.CriticaException;
import br.com.conductor.processamento.repository.cards.PessoaFisicaRepositorio;
import br.com.conductor.processamento.service.validadores.ValidadorService;
import br.com.conductor.processamento.util.Util;

public class SPCBrasilValidadorServiceImpl implements ValidadorService {

	public final static String TIPOREGISTRO_00 = "00";
	public final static String TIPOREGISTRO_01 = "01";
	public final static String TIPOREGISTRO_02 = "02";
	public final static String TIPOREGISTRO_03 = "03";
	
	private final PessoaFisicaRepositorio pessoaFisicaRepositorio;
	
	public SPCBrasilValidadorServiceImpl(PessoaFisicaRepositorio pessoaFisicaRepositorio) {
		this.pessoaFisicaRepositorio = pessoaFisicaRepositorio;
	}

	@Override
	public void validar(String linha) throws CriticaException {
		if (linha != null) {
			String tipoRegistro = linha.substring(0, 2);
			switch (tipoRegistro) {
			case TIPOREGISTRO_00:
				validaHeader(linha);
				break;
			case TIPOREGISTRO_01:
				validarDetalhe01(linha);
				break;
			case TIPOREGISTRO_02:
				validarDetalhe02(linha);
				break;
			case TIPOREGISTRO_03:
				validarTrailler(linha);
				break;
			}
		}
		
		throw new CriticaException("Linha invalida");
	}

	private void validaHeader(String linha) throws CriticaException {
		StringBuilder error = new StringBuilder();

		String dataMovimento = linha.substring(17, 25);
		try {
			Util.getData(dataMovimento);
		}catch (ParseException e) {
			error.append("data inválida ou inexistente ");
		}
			
		if (error.length() != 0) {
			throw new CriticaException(error.toString().trim());
		}
	}
	
	private void validarDetalhe01(String linha) {
		StringBuilder error = new StringBuilder();

		String cpf = linha.substring(10, 55);
		if (Util.check(cpf)) {
			Optional<PessoaFisica> pessoa = this.pessoaFisicaRepositorio.findByCpf(cpf);
			if (!pessoa.isPresent()) {
				error.append("Cpf inexistente : " + cpf);
			}
			
		} else {
			error.append("Cpf Invalido : " + cpf);
		}
		
			
		if (error.length() != 0) {
			throw new CriticaException(error.toString().trim());
		}
	}

	private void validarDetalhe02(String linha) {
		
	}
	
	private void validarTrailler(String linha) {
		
	}
}
