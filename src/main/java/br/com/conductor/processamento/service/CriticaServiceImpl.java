package br.com.conductor.processamento.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.conductor.processamento.domain.api.Arquivo;
import br.com.conductor.processamento.domain.api.Critica;
import br.com.conductor.processamento.repository.api.CriticaRepositorio;

@Service
	
public class CriticaServiceImpl implements CriticaService {
	
	private final CriticaRepositorio criticaRepositorio;
	
	public CriticaServiceImpl(CriticaRepositorio criticaRepositorio) {
		this.criticaRepositorio = criticaRepositorio;
	}

	@Transactional
	public Critica cadastrar(Arquivo arquivo, Integer linha, String mensagem) {
		final Critica critica = new Critica();
		critica.setArquivo(arquivo);
		critica.setLinha(linha);
		critica.setMensagem(mensagem);
		this.criticaRepositorio.save(critica);
		return critica;
	}
}
