package br.com.conductor.processamento.service.validadores;

import br.com.conductor.processamento.exeption.CriticaException;

public interface ValidadorService {
	
	void validar(String linha) throws CriticaException;
}
