package br.com.conductor.processamento.service.validadores;

import br.com.conductor.processamento.domain.api.enuns.TipoArquivo;
import br.com.conductor.processamento.service.validadores.sophus.SophusValidadorServiceImpl;
import br.com.conductor.processamento.service.validadores.spcbrasil.SPCBrasilValidadorServiceImpl;

public class ValidadorFactory {

	public final static String SPC_BRASIL = "SPC_BRASIL";
	public final static String SOPHUS = "SOPHUS";
	public final static String TPC = "TPC";
	public final static String ACP = "ACP";
	
	private final SPCBrasilValidadorServiceImpl spcBrasilValidadorServiceImpl;
	private final SophusValidadorServiceImpl sophusValidadorServiceImpl;
	
	public ValidadorFactory(SPCBrasilValidadorServiceImpl spcBrasilValidadorServiceImpl, SophusValidadorServiceImpl sophusValidadorServiceImpl) {
		this.spcBrasilValidadorServiceImpl = spcBrasilValidadorServiceImpl;
		this.sophusValidadorServiceImpl = sophusValidadorServiceImpl;
	}
	
	public ValidadorService getValidador(TipoArquivo tipo) {
		
		ValidadorService validadorService = null;
		
		switch (tipo.name()) {
	        case SPC_BRASIL:
	        	validadorService = this.spcBrasilValidadorServiceImpl;
	        	break;
	        case SOPHUS:
	        	validadorService = this.sophusValidadorServiceImpl;
	        	break;
	        default:
		}
		
		return validadorService;
	}
	
}
