package br.com.conductor.processamento.exeption;

public class CriticaException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CriticaException() {}

    public CriticaException(String message) {
        super(message);
    }

}
